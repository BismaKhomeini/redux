import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const ShowPage = ({ typedText }) => {
  return (
    <div>
      <h1>Display Page</h1>
      <p>{typedText}</p>
      <br />
      <Link to="/text/input" relative="path">
        Input Page
      </Link>
    </div>
  );
};

ShowPage.propTypes = { typedText: PropTypes.string };

const mapStateToProps = (state) => ({
  typedText: state.text.typedText,
});

export default connect(mapStateToProps)(ShowPage);
