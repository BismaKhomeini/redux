import { connect } from "react-redux";
import actionType from "../../redux/action";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const InputPage = ({ typedText, dispatch }) => {
  const handleChange = (e) => {
    const newText = e.target.value;
    dispatch({ type: actionType.TEXTUPDATE, payload: newText });
  };

  return (
    <div>
      <h1>Typing Page</h1>
      <textarea value={typedText} onChange={handleChange} />
      <br/>
      <Link to="/text/show" relative="path">Show Page</Link>
    </div>
  );
};

InputPage.propTypes = {typedText: PropTypes.string, dispatch: PropTypes.func };

const mapStateToProps = (state) => ({
  typedText: state.text.typedText,
});

export default connect(mapStateToProps)(InputPage);
