import { connect } from "react-redux";
import actionType from "../../redux/action";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import axios from "axios";

const TriggerPage = ({ dispatch }) => {
  const handleFetch = async () => {
    const response = await axios.get(
      "https://jsonplaceholder.typicode.com/users"
    );
    const { data } = response;
    dispatch({ type: actionType.FETCH_USERS, payload: data });
  };
  return (
    <div>
      <button onClick={handleFetch}>Fetch API</button>
      <br/>
      <Link to="/fetch/render" relative="path">
        Render Page
      </Link>
    </div>
  );
};

TriggerPage.propTypes = {
  dispatch: PropTypes.func,
};

const mapStateToProps = () => ({});

export default connect(mapStateToProps)(TriggerPage);
