import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const RenderPage = ({ users }) => {
  console.log(users);
  return (
    <div>
      {users.map((user) => (
        <div key={user.id}>{user.name}</div>
      ))}
      <br />
      <Link to="/fetch/trigger" relative="path">
        Trigger Page
      </Link>
    </div>
  );
};

RenderPage.propTypes = {
  users: PropTypes.array,
};

const mapStateToProps = (state) => ({
  users: state.fetch.users,
});

export default connect(mapStateToProps)(RenderPage);
