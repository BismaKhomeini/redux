import { connect } from "react-redux";
import actionType from "../../redux/action";
import PropTypes from "prop-types";

const CounterPage = ({ count, dispatch }) => {
  return (
    <div>
      <h1>Count: {count}</h1>
      <button
        onClick={() => {
          dispatch({ type: actionType.INCREMENT });
        }}
      >
        Increment
      </button>
      <button
        onClick={() => {
          dispatch({ type: actionType.DECREMENT });
        }}
      >
        Decrement
      </button>
    </div>
  );
};

CounterPage.propTypes = { count: PropTypes.number, dispatch: PropTypes.func };

const mapStateToProps = (state) => ({
  count: state.counter.count,
});

export default connect(mapStateToProps)(CounterPage);
