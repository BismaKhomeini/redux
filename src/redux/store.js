import { createStore, combineReducers } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import countReducer from "./reducers/counterReducer";
import textReducer from "./reducers/textReducre";
import fetchingReducer from "./reducers/fetchingReducer";
const composedEnhancer = composeWithDevTools();

const reducers = combineReducers({
  text: textReducer,
  counter: countReducer,
  fetch: fetchingReducer,
});

const store = createStore(reducers, composedEnhancer);

export default store;




