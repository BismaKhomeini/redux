import actionType from "../action";

const initialState = {
  typedText: "",
};

const textReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.TEXTUPDATE:
      return {
        ...state,
        typedText: action.payload,
      };
    default:
      return state;
  }
};

export default textReducer;
