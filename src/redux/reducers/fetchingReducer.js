import actionType from "../action";

const initialState = {
  users: [],
};

const fetchingReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.FETCH_USERS:
      return {
        ...state,
        users: action.payload,
      };
    default:
      return state;
  }
};

export default fetchingReducer;
