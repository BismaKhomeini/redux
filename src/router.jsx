import { createBrowserRouter, Link } from "react-router-dom";
import CounterPage from "./Pages/CounterPage/CounterPage";
import ShowPage from "./Pages/TextPage/ShowPage";
import InputPage from "./Pages/TextPage/InputPage";
import RenderPage from "./Pages/FetchPage/RenderPage";
import TriggerPage from "./Pages/FetchPage/TriggerPage";

const router = createBrowserRouter([
  {
    path: "/",
    element: (
      <div>
        <h1>Redux</h1>
        <Link to="counter">CounterPage</Link>
        <br />
        <Link to="text/input">Text-Input</Link>
        <br />
        <Link to="text/show">Text-Show</Link>
        <br />
        <Link to="fetch/trigger">Fetch-Trigger</Link> <br />
        <Link to="fetch/render">Fetch-Render</Link>
      </div>
    ),
  },
  {
    path: "counter",
    element: <CounterPage />,
  },
  {
    path: "text/input",
    element: <InputPage />,
  },
  {
    path: "text/show",
    element: <ShowPage />,
  },
  {
    path: "fetch/trigger",
    element: <TriggerPage />,
  },
  {
    path: "fetch/render",
    element: <RenderPage />,
  },
]);

export default router;
